import Text.Printf (printf)
import System.Random (randomRIO)

smallest = 1 :: Integer
biggest = 100 :: Integer

main :: IO ()
main = do
  secret <- randomRIO (smallest, biggest) :: IO Integer
  printf "Find the number between %d and %d.\n" smallest biggest
  loop secret 1 where
    loop :: Integer -> Integer -> IO ()
    loop secret tries = do
      guess <- readLn :: IO Integer
      case compare guess secret of
        LT -> do
          putStrLn "Too small!"
          loop secret $ succ tries
        GT -> do
          putStrLn "Too big!"
          loop secret $ succ tries
        EQ -> do
          printf "You found the number in %d tries!\n" tries
