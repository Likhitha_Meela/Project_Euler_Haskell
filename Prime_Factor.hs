primes = 2 : filter ((==1). length . primeFactors)[3,5..]
primeFactors n = factor n primes
where factor n (p:ps)
| p* p > n = [n]
| n `mod` p == 0 =p :factor (n `div` p) (p:ps)
| otherwise = factor n ps
Prime_Factor = last (primeFactors 600851475143)
main = print $ Prime_Factor
